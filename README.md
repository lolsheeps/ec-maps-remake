# Extinction Curse maps remake

This FoundryVTT module is compilation of maps for Pathfinder2E Extinction Curse Adventure Path. It includes 47 remade from scratch static maps and one animated map. 
All maps are optimized for [FoundryVTT](https://foundryvtt.com/) - they have wall, doors, windows and lighting set up from the box.

Official maps are way too blurry to use with VTT - this project provides convinient high-quality alternative.

Module includes no official AP material and it's impossible to play AP with it. 
If you want to play this amazing adventure you need to [buy official Paizo books](https://paizo.com/store/pathfinder/adventures/adventurePath/extinctioncurse).

# Contrubution

If you made additional redrawn EC maps and want to share them with the community - please contact me via issues or Discord. I'll gladly add your maps and expand this module for everyone to enjoy. 

# Content

Module currently covers all maps in books 1-4 and maps from the beginning of book 5.

# Maps creators

This module wouldn't be possible without amazing map creators. 

**Lios** ([Patreon Link](https://www.patreon.com/liospc)) made maps for first book.

**Robster** made maps for books 2-5. 

**Luebbi** made maps for book 3,4,5. 

All maps without "by xxx" in their name are made by mapmaker in compendium title. 

# Installation

Go for Foundry Add-on modules, press "Install module" and search this module by name. Enable module in world settings and maps will appear in compendiums. Alternatively use this link:

```
https://gitlab.com/Ustin/ec-maps-remake/-/raw/master/module.json
```

[![Sample Map](readme_pics/madmug.jpg)](https://gitlab.com/Ustin/ec-maps-remake/-/raw/master/maps/madmug.webp)


